package com.platform.exception;

import com.platform.enums.ResultCodeEnum;
import lombok.Data;

/**
 * @ClassName BusinessException
 * @Description 业务处理异常
 * @Author yoki
 * @Date 2021-01-08 12:00
 * @Vsersion 1.0
 */
@Data
public class BusinessException extends RuntimeException {
    private int code;

    private String msg;

    public BusinessException() {
        this.code = ResultCodeEnum.FAILED.code;
        this.msg = ResultCodeEnum.FAILED.msg;
    }

    public BusinessException(String message) {
        this.code = ResultCodeEnum.FAILED.code;
        this.msg = message;
    }

    public BusinessException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
