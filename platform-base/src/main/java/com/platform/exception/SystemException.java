package com.platform.exception;

import com.platform.enums.ResultCodeEnum;
import lombok.Data;

/**
 * @ClassName SystemException
 * @Description 系统处理异常
 * @Author yoki
 * @Date 2021-01-08 13:37
 * @Vsersion 1.0
 */
@Data
public class SystemException extends RuntimeException {
    private int code;

    private String msg;

    public SystemException() {
        this.code = ResultCodeEnum.ERROR.code;
        this.msg = ResultCodeEnum.ERROR.msg;
    }

    public SystemException(String message) {
        this.code = ResultCodeEnum.ERROR.code;
        this.msg = message;
    }

    public SystemException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public SystemException(Throwable cause) {
        super(cause);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
