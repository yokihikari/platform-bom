package com.platform.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName RibbonConfig
 * @Description TODO
 * @Author yoki
 * @Date 2021-01-11 16:28
 * @Vsersion 1.0
 */
@Configuration
public class RibbonConfig {
    /**
     * 负载均衡策略配置
     * @return
     */
    public IRule rule(){
        // 随机策略 从所有可用的提供者中随机选择一个
        return new RandomRule();
    }
}
